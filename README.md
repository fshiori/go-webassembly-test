Run:

```sh
$ GOARCH=wasm GOOS=js go build -o test.wasm main.go
```

And copy over the HTML & JS support files:

```sh
$ cp $(go env GOROOT)/misc/wasm/wasm_exec.{html,js} .
```

Then serve those three files (wasm_exec.html, wasm_exec.js, and test.wasm) to a web browser.